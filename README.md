# Spring Data REST Calls

This sample application demonstrates how Spring Data REST provides paging and sorting functions. This uses
the following resources:

* Spring Boot 2.1.11
* Spring Data JPA
* Spring REST Repositories
* Spring HATEOAS
* Swagger

It also has Thymeleaf support, but no templates are currently included.

> This application uses Spring Boot 2.1.11 due to [known incompatibilities](https://github.com/springfox/springfox/issues/2932) between the
> Spring Boot 2.2._x_ release and Swagger Data REST 3.0.0-SNAPSHOT. Once those are resolved this should be updated to use the latest version
> of Spring Boot 2.2._x_.
 
## Running

You can run this application from the command line:

```bash
$ ./gradlew bootRun

> Task :bootRun

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::       (v2.1.11.RELEASE)

2020-01-03 16:09:24.836  INFO 73736 --- [           main] o.n.x.d.t.XnatDataTablesApplication      : Starting XnatDataTablesApplication...
``` 

You can verify that the application is running and listening with this command:

```bash
$ http http://localhost:8080
HTTP/1.1 200
Connection: keep-alive
Content-Type: application/hal+json;charset=UTF-8
Date: Fri, 03 Jan 2020 22:13:03 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "_links": {
        "accidents": {
            "href": "http://localhost:8080/accidents{?page,size,sort}",
            "templated": true
        },
        "profile": {
            "href": "http://localhost:8080/profile"
        }
    }
}
```

## Fun and Games

The nice thing here is that I don't _think_ anyone can lose an eye with these particular games.

> The sample commands below use **jq** to trim the output. If you remove the `| jq ...` part of the commands,
> you'll get the full JSON output. Because of the HATEOAS metadata, this can get pretty large and complex pretty
> quickly, which isn't a problem for code consuming the API response but does look awful in a README file!

The most basic call is just a direct GET to the **/accidents** endpoint:

```bash
$ http http://localhost:8080/accidents | jq '._embedded.accidents[].accidentId'
"A-1"
"A-2"
"A-3"
"A-4"
"A-5"
"A-6"
"A-7"
"A-8"
"A-9"
"A-10"
"A-11"
"A-12"
"A-13"
"A-14"
"A-15"
"A-16"
"A-17"
"A-18"
"A-19"
"A-20"
```

There are only 20 entries that show up here but there are actually 2,000 in the database. This demonstrates the default paging in action:
by default you get page 0 with a page size of 20 (these can be changed with configuration options). Now try adding a page specifier:

```bash
$ http http://localhost:8080/accidents page==1 | jq '._embedded.accidents[].accidentId'
"A-21"
"A-22"
"A-23"
"A-24"
"A-25"
"A-26"
"A-27"
"A-28"
"A-29"
"A-30"
"A-31"
"A-32"
"A-33"
"A-34"
"A-35"
"A-36"
"A-37"
"A-38"
"A-39"
"A-40"
```

There are still just 20 entries but they're not the same ones as the first query. Now try adding a page size:

```bash
$ http http://localhost:8080/accidents page==4 size==10 | jq '._embedded.accidents[].accidentId'
"A-41"
"A-42"
"A-43"
"A-44"
"A-45"
"A-46"
"A-47"
"A-48"
"A-49"
"A-50"
```

Now sort them:

```bash
$ http http://localhost:8080/accidents page==4 size==10 sort==accidentId | jq '._embedded.accidents[].accidentId'
"A-1034"
"A-1035"
"A-1036"
"A-1037"
"A-1038"
"A-1039"
"A-104"
"A-1040"
"A-1041"
"A-1042"
```

Wild. Because the accident ID property is a string, the ordering changes the results for the same page as the previous call. Now
change the sort order:

```bash
$ http http://localhost:8080/accidents page==4 size==10 sort==accidentId,desc | jq '._embedded.accidents[].accidentId' | less
"A-962"
"A-961"
"A-960"
"A-96"
"A-959"
"A-958"
"A-957"
"A-956"
"A-955"
"A-954"
```
