package org.nrg.xnat.data.tables.data.repositories;

import io.swagger.annotations.Api;
import java.util.List;
import org.nrg.xnat.data.tables.data.entities.Accident;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Api(tags = "accidents")
@RepositoryRestResource
public interface AccidentRepo extends JpaRepository<Accident, Long> {
    Accident findById(final long id);

    Accident findByAccidentId(final String accidentId);

    List<Accident> findAllByZipCode(final String zipCode);

    List<Accident> findAllByPrecipitation(final String precipitation);
}
