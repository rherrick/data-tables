package org.nrg.xnat.data.tables.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
@Slf4j
public class DataTablesRepositoryRestConfigurer implements RepositoryRestConfigurer {
    @Autowired
    public DataTablesRepositoryRestConfigurer(final RepositoryRestProperties properties, final EntityManager entityManager, final @Autowired(required = false) Jackson2ObjectMapperBuilder builder) {
        _properties = properties;
        _entityManager = entityManager;
        _builder = builder;
    }

    @Override
    public void configureRepositoryRestConfiguration(final RepositoryRestConfiguration configuration) {
        _properties.applyTo(configuration);
        configuration.exposeIdsFor(_entityManager.getMetamodel().getEntities().stream().map(Type::getJavaType).toArray(Class[]::new));
    }

    @Override
    public void configureJacksonObjectMapper(final ObjectMapper mapper) {
        if (_builder != null) {
            _builder.configure(mapper);
        }
    }

    private final RepositoryRestProperties    _properties;
    private final EntityManager               _entityManager;
    private final Jackson2ObjectMapperBuilder _builder;
}
