package org.nrg.xnat.data.tables.data.entities;

import java.util.Date;
import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Accident extends BaseMetadata {
    private String  accidentId;
    private String  source;
    private float   tmc;
    private int     severity;
    private Date    startTime;
    private Date    endTime;
    private Double  latitude;
    private Double  longitude;
    private float   distance;
    private String  description;
    private String  number;
    private String  street;
    private String  side;
    private String  city;
    private String  county;
    private String  state;
    private String  zipCode;
    private String  country;
    private String  timezone;
    private String  airportCode;
    private Date    weatherTimestamp;
    private float   temperature;
    private float   windChill;
    private float   humidity;
    private float   pressure;
    private float   visibility;
    private String  windDirection;
    private String  windSpeed;
    private String  precipitation;
    private String  weatherCondition;
    private boolean amenity;
    private boolean bump;
    private boolean crossing;
    private boolean giveWay;
    private boolean junction;
    private boolean noExit;
    private boolean railway;
    private boolean roundabout;
    private boolean station;
    private boolean stop;
    private boolean trafficCalming;
    private boolean trafficSignal;
    private boolean turningLoop;
    private String  sunriseSunset;
    private String  civilTwilight;
    private String  nauticalTwilight;
    private String  astronomicalTwilight;
}
